Java 11

- Hi, this is my `Plagiarist` solution
- I would like to leave some notes :)

1. I decided to parallel via Stream API, i don't why, i just thought that it is good idea (i still thing so);
2. In PlagiaristHandlerImpl you might see huge `Response` wrapers with null `isExist` check, `Response` relate to http and maybe redundant in this task;
3. For Banchmark used `jmh-core`, avg boost with `.parallel()` 300%
4. It was my challenge to do this task without help and from another side i relate to this task like to part of education;
5. I would be grateful for any criticism;
6. Sorry if you expected multitread realization on `Executor service` level, i may do it later;
