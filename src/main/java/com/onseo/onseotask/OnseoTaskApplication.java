package com.onseo.onseotask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class OnseoTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnseoTaskApplication.class, args);
	}

}
