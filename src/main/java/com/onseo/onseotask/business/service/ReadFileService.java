package com.onseo.onseotask.business.service;

import com.onseo.onseotask.entities.web.Response;

import java.io.File;
import java.util.List;

/**
 * Created by Alexey Podgorny on 26.12.2020.
 */
public interface ReadFileService {

    Response<File> getFileByName(String fileName);

    Response<List<File>> getAllFilesByNames(String... fileNames);
}
