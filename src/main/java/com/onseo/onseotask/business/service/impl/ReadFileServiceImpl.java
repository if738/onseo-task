package com.onseo.onseotask.business.service.impl;

import com.onseo.onseotask.business.service.ReadFileService;
import com.onseo.onseotask.entities.web.Error;
import com.onseo.onseotask.entities.web.Response;
import com.onseo.onseotask.entities.web.enums.BasicErrorSubType;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Alexey Podgorny on 26.12.2020.
 */

@Slf4j
@Component
@RequiredArgsConstructor
public class ReadFileServiceImpl implements ReadFileService {

    @Override
    public Response<File> getFileByName(String fileName) {
        try {
            return Response.of(ResourceUtils.getFile("classpath:texts/" + fileName));
        } catch (FileNotFoundException e) {
            log.error("getFileByName. Can't find file, fileName={} error={}", fileName, e.getMessage());
            String errorType = BasicErrorSubType.API_ERROR.getErrorType();
            String subErrorType = BasicErrorSubType.API_ERROR.toString();
            return Response.of(Error.of(errorType, subErrorType, "File not found"));
        }
    }

    @Override
    public Response<List<File>> getAllFilesByNames(String... fileNames) {
        List<File> files = new ArrayList<>();
        for (String fileName : fileNames) {
            Response<File> fileByName = getFileByName(fileName);
            if (fileByName.isSuccess()) files.add(fileByName.getData());
        }
        return Response.of(files);
    }

}
