package com.onseo.onseotask.business.handler;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Alexey Podgorny on 27.12.2020.
 */
public interface WordHandler {

    Map<String, Long> getAllIntersectionsCount(Set<String> targetText, List<File> anotherFiles);

    Set<String> getWordsSet(File files);

    Long getAllWordsCount(File file);
}
