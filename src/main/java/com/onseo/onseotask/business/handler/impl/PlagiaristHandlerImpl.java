package com.onseo.onseotask.business.handler.impl;

import com.onseo.onseotask.business.handler.PlagiaristHandler;
import com.onseo.onseotask.business.handler.WordHandler;
import com.onseo.onseotask.business.service.ReadFileService;
import com.onseo.onseotask.entities.business.Plagiarist;
import com.onseo.onseotask.entities.web.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Alexey Podgorny on 27.12.2020.
 */

@Slf4j
@Component
@RequiredArgsConstructor
public class PlagiaristHandlerImpl implements PlagiaristHandler {

    private final ReadFileService readFileService;
    private final WordHandler wordHandler;

    public Response<List<Plagiarist>> compareOneWithAnother(String targetFileName, String... secondaryFileNames) {
        log.trace("compareOneWithAnother. Started, targetFileName={} secondaryFileNames={}", targetFileName, secondaryFileNames);
        List<Plagiarist> plagiarists = new ArrayList<>();

        Response<File> targetFileResponse = readFileService.getFileByName(targetFileName);
        if (targetFileResponse.notSuccess()) return Response.of(targetFileResponse.getError());
        Response<List<File>> anotherFilesResponse = readFileService.getAllFilesByNames(secondaryFileNames);
        if (anotherFilesResponse.notSuccess()) return Response.of(anotherFilesResponse.getError());

        File targetFile = targetFileResponse.getData();
        Set<String> targetFilesSet = wordHandler.getWordsSet(targetFile);
        List<File> anotherFiles = anotherFilesResponse.getData();
        Map<String, Long> allIntersections = wordHandler.getAllIntersectionsCount(targetFilesSet, anotherFiles);

        for (Map.Entry<String, Long> entry : allIntersections.entrySet()) {
            String secondaryFileName = entry.getKey();
            Long countIntersections = entry.getValue();
            Response<File> secondaryFile = readFileService.getFileByName(secondaryFileName);
            if (targetFileResponse.notSuccess()) continue;
            Long secondaryTotalWords = wordHandler.getAllWordsCount(secondaryFile.getData());
            double percentage = calculatePercentage(countIntersections, secondaryTotalWords);
            plagiarists.add(Plagiarist.of(targetFileName, secondaryFileName, percentage));
        }

        print(plagiarists);

        log.debug("compareOneWithAnother. Finished, targetFileName={} secondaryFileNames={}", targetFileName, secondaryFileNames);
        return Response.of(plagiarists);
    }

    //Invokes every 15 second
    @Scheduled(fixedRate = 15_000)
    public void init() {
        compareOneWithAnother("1.txt", "2.txt", "3.txt", "4.txt", "5.txt", "6.txt");
    }

    private void print(List<Plagiarist> plagiarists) {
        for (Plagiarist p : plagiarists) {
            String sb = "Text " +
                    p.getTargetText() +
                    " is " +
                    p.getSimilarityPercentage() +
                    "% similar to text " +
                    p.getComparedWithText();
            System.out.println(sb);
        }
    }

    private double calculatePercentage(Long obtained, Long total) {
        return obtained.doubleValue() * 100 / total.doubleValue();
    }

}
