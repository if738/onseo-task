package com.onseo.onseotask.business.handler.impl;

import com.onseo.onseotask.business.handler.WordHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Alexey Podgorny on 27.12.2020.
 */

@Slf4j
@Component
@RequiredArgsConstructor
public class WordHandlerImpl implements WordHandler {

    private final static Pattern allAlphabeticalPattern = Pattern.compile("[а-яА-Яa-zA-Z]+");

    @Override
    public Map<String, Long> getAllIntersectionsCount(Set<String> targetText, List<File> anotherFiles) {
        log.trace("getAllIntersectionsCount. Started");
        HashMap<String, Long> mappedIntersectedWordsI = new HashMap<>();

        List<Long> intersectedWords = anotherFiles.stream()
                .parallel()
                .map(this::tryToGetText)
                .map(s -> this.groupWordsFromLine(s, 3))
                .map(HashSet::new)
                .map(l -> this.getIntersectedWords(Arrays.asList(l, targetText)))
                .map(v -> (long) v.size())
                .collect(Collectors.toList());

        Iterator<String> anotherFilesI = anotherFiles.stream()
                .map(File::getName)
                .collect(Collectors.toList()).iterator();
        Iterator<Long> intersectedWordsI = intersectedWords.iterator();
        while (anotherFilesI.hasNext() || intersectedWordsI.hasNext()) {
            mappedIntersectedWordsI.put(anotherFilesI.next(), intersectedWordsI.next());
        }

        log.debug("getAllIntersectionsCount. Finished, mappedIntersectedWordsI={}", mappedIntersectedWordsI);
        return mappedIntersectedWordsI;
    }

    @Override
    public Set<String> getWordsSet(File file) {
        return Optional.of(file)
                .stream()
                .parallel()
                .map(this::tryToGetText)
                .map(s -> this.groupWordsFromLine(s, 3))
                .flatMap(List::stream)
                .collect(Collectors.toSet());
    }

    @Override
    public Long getAllWordsCount(File file) {
        return Optional.of(file)
                .map(this::tryToGetText)
                .stream()
                .peek((mock) -> log.trace("getAllWordsCount. Started, file={}", file))
                .parallel()
                .map(s -> this.groupWordsFromLine(s, 3))
                .mapToLong(List::size)
                .peek(c -> log.debug("getAllWordsCount. Finished, count={}", c))
                .sum();
    }

    private Set<String> getIntersectedWords(List<Set<String>> words) {
        return words.stream()
                .parallel()
                .reduce((p, c) -> {
                    p.retainAll(c);
                    return p;
                }).orElse(Collections.emptySet());
    }

    private Optional<FileInputStream> getFileInputStream(File f) {
        try {
            return Optional.of(new FileInputStream(f));
        } catch (Exception e) {
            log.error("methodName. Can't read current file={} error={}", f, e.getMessage());
            return Optional.empty();
        }
    }

    private String tryToGetText(File file) {
        Optional<FileInputStream> fileInputStreamO = this.getFileInputStream(file);
        if (fileInputStreamO.isEmpty()) return "";
        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStreamO.get());
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String rawText = bufferedReader.lines().collect(Collectors.joining(" "));
        int detectedWordsPercentage = getDetectedWordsPercentage(rawText);
        if (detectedWordsPercentage > 90) return rawText;
        Optional<FileInputStream> secondFileInputStreamO = this.getFileInputStream(file);
        if (secondFileInputStreamO.isEmpty()) return "";
        inputStreamReader = new InputStreamReader(secondFileInputStreamO.get(), StandardCharsets.UTF_8);
        bufferedReader = new BufferedReader(inputStreamReader);
        return bufferedReader.lines().collect(Collectors.joining(" "));
    }

    private List<String> groupWordsFromLine(String line, int maxWordLength) {
        List<String> list = new ArrayList<>();
        Matcher matcher = allAlphabeticalPattern.matcher(line);
        while (matcher.find()) {
            String group = matcher.group(0);
            if (group.length() <= maxWordLength) continue;
            list.add(group);
        }
        return list;
    }

    private int getDetectedWordsPercentage(String text) {
        List<String> list = groupWordsFromLine(text, 0);
        int detectedWordsLengthWithoutSpaces = String.join("", list).length();
        int rawTextLengthWithoutSpaces = text.replaceAll("\\s", "").length();
        return calculatePercentage(detectedWordsLengthWithoutSpaces, rawTextLengthWithoutSpaces);
    }

    private int calculatePercentage(double obtained, double total) {
        return Double.valueOf(obtained * 100 / total).intValue();
    }

}
