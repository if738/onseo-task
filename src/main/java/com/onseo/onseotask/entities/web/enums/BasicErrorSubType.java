package com.onseo.onseotask.entities.web.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by Alexey Podgorny on 21.12.2020.
 */
@Getter
@AllArgsConstructor
public enum BasicErrorSubType {

    UNEXPECTED_ERROR(BasicErrorType.UNEXPECTED_ERROR),
    API_ERROR(BasicErrorType.API_ERROR),
    PARSING_ERROR(BasicErrorType.UNEXPECTED_ERROR),

    INVALID_TIME(BasicErrorType.VALIDATION_ERROR),
    INVALID_NUMBER(BasicErrorType.VALIDATION_ERROR);

    private final String errorType;

}
