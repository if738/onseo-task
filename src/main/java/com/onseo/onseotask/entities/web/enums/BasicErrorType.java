package com.onseo.onseotask.entities.web.enums;

/**
 * Created by Alexey Podgorny on 21.12.2020.
 */
public interface BasicErrorType {

    String UNEXPECTED_ERROR = "UNEXPECTED_ERROR";
    String API_ERROR = "UNEXPECTED_ERROR";
    String VALIDATION_ERROR = "VALIDATION_ERROR";

}
