package com.onseo.onseotask.entities.web;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import static com.fasterxml.jackson.annotation.JsonCreator.Mode.PROPERTIES;

@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class Error {

    private final String type;
    private final String subType;
    private final String description;

    @JsonCreator(mode = PROPERTIES)
    public static Error of(@JsonProperty("type") String type,
                           @JsonProperty("subtype") String subType,
                           @JsonProperty("description") String description) {
        return new Error(type, subType, description);
    }

}
