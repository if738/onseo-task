package com.onseo.onseotask.entities.business;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by Alexey Podgorny on 27.12.2020.
 */
@Data
@AllArgsConstructor(staticName = "of")
public class Plagiarist {

    private String targetText;
    private String comparedWithText;
    private Double similarityPercentage;

}
